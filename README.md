**PROGETTO PR2 2015**

				Matricola	Cognome			Nome
Autore1		49058			Uccheddu			Maria Cristina
Autore2		48980			Moi				Francesca
Autore3		49049			Massa				Marco


				Nome						Categoria
Funzione:
-Semplice	ARROTONDA.PER.ECC		MATEMATICA
-Complessa	AMMORT					FINANZA
-Custom		CreateFriendship		SOCIAL


Tutte e tre le funzioni sono state collaudate e funzionano.
Tutte e tre contengono al proprio interno il main per poterle collaudare anche tramite il terminale.
Le parti del main sono commentate per poterle collaudare togliere il commento.

Dal terminale per compilare le classi posizionarsi all'interno della cartella java 
del progetto2015 (l'interfaccia SheetFunction è già compilata):

javac it/unica/pr2/progetto2015/g49058_48980_49049/Semplice.java
javac it/unica/pr2/progetto2015/g49058_48980_49049/Complessa.java
javac -cp ../libs/twitter4j-core-4.0.4.jar:. it/unica/pr2/progetto2015/g49058_48980_49049/Custom.java 

Per eseguire le classi:
java it.unica.pr2.progetto2015.g49048_48980_49049.Semplice
java it.unica.pr2.progetto2015.g49048_48980_49049.Complessa
java -cp ../libs/twitter4j-core-4.0.4.jar:. it/unica/pr2/progetto2015/g49058_48980_49049/Custom.java 


		FUNZIONE SEMPLICE

Implementa la funzione "ARROTONDA.PER.ECC" di LibreOffice.


-REALIZZAZIONE

L'arrotondamento del numero avviene attraverso vari passaggi:

-si assegna ad una variabile di appoggio (app1) il valore assoluto del numero (che viene passato come parametro, 
 inserito nella variabile num) che si vuole arrotondare tramite il metodo abs della classe Math

-in una seconda variabile di appoggio (app2) si assegna il valore di 10 elevato la cifra a cui si vuole arrotondare
 tramite il metodo pow della classe Math

-la variabile app1 prende il valore arrotondato 

-controllo se il numero che si vuole arrotondare è positivo o negativo, nel caso in cui sia negativo
 si rende negativa la variabile di appoggio app1 

-la variabile num prende il valore della variabile d'appoggio app1

-a questo punto si restituisce la variabile num che contiene il valore del numero arrotondato


-RETURN

Restituisce il numero arrotondato alla cifra in cui si è imposto di arrotondare.


-foglio.ods

Inserire nella cella affianco a quella che contiene la scritta Numero il numero che si desidera arrotondare (cella C20). 
Nella sottostante (cella C21) inserire il numero della cifra a cui si vuole arrotondare.

Precisazioni:

	- in un numero 123,45
	se si desidera arrotondare la parte intera si userà un valore negativo, a parte per l'unità che è 0
	se si desidera arrotondare la parte decimale si userà un valore positivo.

	ESEMPIO	
	se si desidera arrotondare alla cifra -3, il valore restituito di num sarà 1000
													  -2												200
													  -1												130	
														0												124
														1												123,5
														2												123,45
														3												123,45

Se si desidera inserire numeri decimali, utilizzare la virgola e non il punto.



		FUNZIONE COMPLESSA

Implementa la funzione "AMMORT" di LibreOffice.

La seguente funzione calcola l'ammortamento degressivo di un bene per un periodo specificato.
Il valore di ammortamento è più alto all'inizio dell'ammortamento e si riduce da un periodo di ammortamento all'altro. 
Con questa forma di calcolo il valore contabile non diventa mai zero: diminuisce fino al valore residuo indicato. 

Per la corretta esecuzione della funzione, occore:
1. Inserire i numeri separando la parte intera da quella decimale con la virgola e non col punto;
2. Non inserire il separatore delle migliaia (si inserisce automaticamente)
3. Se si vuole calcolare l'ammortamento usando un unità diversa dall'anno, quindi in giorni o mesi, bisogna inserire nella vita utile il
   valore in anni moltiplicato rispettivamente per 12 o per 365; 
4. E' obbligatorio inserire un valore in tutti i campi tranne in fattore;
5. Nel campo fattore se si vuole lasciarlo vuoto è necessario inserire uno spazio vuoto o la scritta "no".


-REALIZZAZIONE

L'implementazione di questa funzione avviene attraverso alcuni passaggi:
- vengono prima di tutto eseguiti alcuni controlli sui parametri presi in input:
	* controllo sul campo fattore per verificare se il campo è vuoto o meno o sia presente la scritta "no": 
	  -in caso di campo vuoto o di scritta "no" viene dato a fattore il valore 2; 
	* controllo ulteriore sul campo fattore per verificare che non sia minore o uguale a zero 
	  in caso contrario viene segnalato un errore;
	  -in caso di valore valido si assegna il valore a fattore;
	* controllo su tutti gli altri parametri presi passati alla funzione dall'utente per verificare che siano tutti superiori a 0 
	  (il campo valRes che può valere anche 0); in caso contrario viene segnalato l'errore;
	* confronto di valRes con costo: valRes dev'essere per forza inferiore o uguale a costo altrimenti viene segnalato l'errore;
	* controllo di periodo e vitaUtile: periodo non deve essere superiore a vitaUtile altrimenti viene segnalato l'errore;
- dopodichè viene implementato un ciclo for da 0 a periodo (escluso); il ciclo si ferma dunque una volta raggiunto il periodo desiderato:
- all'interno del for viene calcolato ad ogni incremento di i (e quindi del periodo) l'ammortamento annuo utilizzando l'espressione che
  restituisce il risultato minimo tra questi: 
  * (costo - totAmmort) * (fattore/vitaUtile);
  * (costo - valRes - totAmmort); 
- il minimo viene individuato mediante il metodo min della libreria java.lang.Math;
- viene usato questo procedimento poichè quando il valore ottenuto dalla seconda formula sarà inferiore a quello ottenuto dalla prima
  vorrà dire che si è arrvati all'ultimo ciclo in cui l'ammortamento deve essere calcolato tenendo conto del valore residuo inserito
  dall'utente. 
- ad ogni ciclo viene inoltre aggiornato totAmmort, che rappresenta il fondo ammortamento, sommando di periodo in periodo il nuovo
  ammortamento.
- Il valore ottenuto viene preso da una variabile di tipo String valoreAmmort

-RETURN

Restituisce l'ammortamento degressivo aritmetico per un periodo specificato o quando si verifica un errore 
la stampa della descrizione di esso.

-foglio.ods

Inserire nella cella affianco a quella che contiene la scritta Costo il costo iniziale del bene da amortizzare (cella C4). 
Nelle sottostanti (celle C5, C6, C7) inserire rispettivamente il valore residuo, la vita utile del bene, il periodo del quale 
si vuole l'ammortamento.
Nella cella C8 inserire il fattore ossia il tasso di deprezzamento del valore residuo, siccome questo è un valore facoltativo:
- si può inserire uno spazio vuoto o stringa "no", per indicare che non lo si vuole specificare e di default 
  il fattore verrà considerato come 2.
- altrimenti si può inserire un valore maggiore di zero.


		FUNZIONE CUSTOM

La funzione Custom da noi scelta "createFriendship" permette a un utente predefinito creato da noi 
di seguire un determinato utente su Twitter sia che questo sia collegato o meno a Twitter.

-INFORMAZIONI ACCOUNT TWITTER

Email: ubaldoboldi@hotmail.it
Nome utente: ubaldoboldi
Password: BoldiubaldO80

La funzione restituisce una stringa che ti informa che segui quel determinato utente Twitter.

I permessi sono incorporati alla struttura del progetto.

I permessi sono contenuti in due file chiamati twitter4j.properties che risiedono:
- il primo nella root ovvero uno si trova all'interno della cartella java iniziale 
  
- il secondo risiede nella stessa directory dove è presente la funzione Custom implementata in Custom.java 
  che si trova nel percorso se ci troviamo all'interno della directory progetto2015 : 
  java/it/unica/pr2/progetto2015/g49058_48980_49049. Il file si troverà all'interno della cartella g49058_48980_49049

*Note utilizzo su foglio.ods*

Inserire nella cella C4 il nome utente Twitter dell'utente che si vuole seguire privato del carattere @.
Se non si inserisce un nome o il nome utente inserito è inesistente il valore restituito sarà "Nome non valido".
Se stai già seguendo l'utente inserito il valore restituito sarà "Stai gia seguendo utenteNomeTwitter".



