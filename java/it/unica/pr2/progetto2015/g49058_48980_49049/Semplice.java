/*
 * 	FUNZIONE SEMPLICE - ARROTONDA.PER.ECC
 */

package it.unica.pr2.progetto2015.g49058_48980_49049;
import it.unica.pr2.progetto2015.interfacce.SheetFunction;


public class Semplice implements SheetFunction{
    	
	//Costruttore vuoto
	public Semplice(){}
	
	/*
	 *	Arrotonda il valore di un numero per eccesso.
	 *	@param args args[0] numero che si desidera arrotondare
	 *		    args[1] numero della cifra a cui si vuole arrotondare: - se positiva la cifra sarà dalla parte decimale
	 *									   - se negativa la cifra sarà dalla parte intera
	 *
	 *	@return Valore del numero passato come parametro arrotondato per eccesso
	 *S
	 */
	@Override
	public Object execute(Object... args)
	{
		//VARIABILI
		
		try
		{
			Double num = (Double)args[0];
			Integer val = (Integer)args[1];
			Double app1, app2;

			String numero = "" + num;
			Integer virgola;		//la variabile prenderà come valore la posizione del punto nel numbero
			Integer parteDecimale;  	//la variabile prenderà come valore la lunghezza della parte decimale


			//Calcolo della posizione del punto nel numero, viene incrementata di uno per poter eseguire il calcolo successivo delle cifre decimali
			virgola = numero.indexOf('.') + 1; 

			//Calcolo delle cifre della parte decimale
			parteDecimale = (Integer) ( numero.length() - virgola );	
			
	      /*	
	       *	L'arrotondamento viene effettuato quando il numero delle cifre della parte decimale
	       *	è maggiore del valore della cifra passata come parametro alla quale si vuole arrotondare
	       *	
	       *	ES. ARROTONDA.PER.ECC(23,143;2) darà come risultato 23,15
	       *	    ARROTONDA.PER.ECC(23,143;-2) darà come risultato 100
	       *	
	       *	lo stesso avviene per i numeri negativi grazie alle variabili di appoggio app1 e app2
	       *	
	       *	Gli unici casi in cui non deve effettuare alcun calcolo per arrotondare e si restituisce il numero stesso è quando:
	       *	  - la cifra indicata che si desidera arrotondare il numero è maggiore del numero delle cifre decimali  
			    ES ARROTONDA.PER.ECC(23,143;5) darà come risultato 23,143
	       *	  - Quando il numero non ha valore decimale e la cifra a cui si vuole arrotondare è zero (ovvero all'unità)
			    ES ARROTONDA.PER.ECC(23,143;0) darà come risultato 23,143
	       */
			if(parteDecimale > val) 
			{

				app1 = Math.abs(num);
				app2 = Math.pow(10, val);            
				app1 = Math.ceil(app1 * app2) / app2;

				if(num<0)
				{
					app1 = -app1;
				}

				num = app1;
			}
		      
			return num;
		 }
		 catch(ClassCastException e)
		 {
			return "errore";
		 }
	}

	/*** MAIN - per testare la funzione fuori dal foglio LibreOffice ***/

	/*   
	public static void main(String[] args) {

		Double num = 13.245;
		Integer val = -2;
		Double risult;
		Semplice semplice = new Semplice();  

		risult = (Double) semplice.execute(num, val);

		System.out.println("Categoria: " + semplice.getCategory());
        	System.out.println("Aiuto: " + semplice.getHelp());
       		System.out.println("Nome: " + semplice.getName());        
        	System.out.println("Il valore arrotondato è " + risult);

	}
	*/


	/** 
	Restituisce la categoria LibreOffice;
	Vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
	ad esempio, si puo' restituire "Data&Orario" oppure "Foglio elettronico"
	*/
	@Override
	public String getCategory()
	{
		return "Matematica";
	}

	/** Informazioni di aiuto */
	@Override
	public String getHelp()
	{
		return "Restituisce Numero arrotondato per eccesso.";
	}
	
	/** 
	Nome della funzione.
	vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
	ad es. "VAL.DISPARI" 
	*/
	@Override         
	public String getName()
	{
		return "ARROTONDA.PER.ECC";
	}

}
