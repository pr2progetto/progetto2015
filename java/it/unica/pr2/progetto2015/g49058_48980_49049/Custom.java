/*
 * FUNZIONE CUSTOM - Permette di seguire una persona, il cui nome è passato come parametro nel metodo execute
 */

package it.unica.pr2.progetto2015.g49058_48980_49049;

import it.unica.pr2.progetto2015.interfacce.SheetFunction;

/* IMPORTIAMO LE LIBRERIE DI TWITTER */
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.api.FriendsFollowersResources;
import twitter4j.Relationship;


public class Custom implements SheetFunction {

	//Costruttore vuoto
	public Custom(){}

	/*
 	 *	Consente di seguire una persona specifica su Twitter.
	 *	@param args args[0] nome della persona che si desidera seguire
	 *	@return Restituisce una stringa in cui ti avvisa che ora segui quel determinato utente su Twitter
	 *
	 */
	@Override
	public Object execute(Object... args)
	{		 
		 		
		Twitter twitter = TwitterFactory.getSingleton();    

		/* il metodo friendsFollowers ci restituisce un oggetto di tipo friendsFollowerResources */
		FriendsFollowersResources follower = twitter.friendsFollowers();
		
		User user; /* conterrà il riferimento a un oggetto della classe User che conterrà le informazioni di base dell'utente 
						* una volta seguito */ 
		Relationship rel; /* conterrà il riferimento a un oggetto della classe Relationship */
		Boolean bool = true;
		       
		try
		{    
			/* Il metodo showFriendship restituisce un oggetto Relationship contenente le informazioni dettagliate sulla relazione 
			 * tra noi (ubaldoboldi) e la persona che vogliamo seguire */
			rel = follower.showFriendship("ubaldoboldi", (String)args[0]);

			/* Controlliamo poi se l'utente inserito è già seguito oppure no */
			if (rel.isSourceFollowingTarget() == false)
			{
				 user = follower.createFriendship((String)args[0]); /* se ancora non lo seguiamo allora richiamo il metodo 
				 						     * createFriendship per seguire l'utente */
				 																	  
				 bool = false; // diamo a bool il valore false che ci servirà nel prossimo controllo
			}    
		}
		catch( TwitterException te )
		{
		   /* Se si verifica una TwitterException allora il nome non è valido e quindi segnaliamo l'errore */ 
			return (Object)"Nome non valido";
		}


		/* Controlliamo se la variabile bool vale ancora true o è stata modificata a false; se è stata modificata a false vuol dire 
		 * che non seguivamo ancora l'utente inserito mentre se è ancora true vuol dire che l'utente inserito è già stato seguito */
		if(bool == true)
			return (Object) ("Stai già seguendo " + args[0]); // notifichiamo con un messaggio che stiamo già seguendo quell'utente
		else
			return (Object)("Ora segui " + args[0]) ; /* notifichiamo con un messaggio che la funzione è andata a buon fine e 
         														 * che quindi ora seguiamo l'utente inserito */

	}
	
	
	/*** MAIN - per testare la funzione fuori dal foglio LibreOffice ***/

	/*
	public static void main(String[] args)
	{
		Object o;
		Custom custom = new Custom();
		o = custom.execute("ubaldaboldi");
		System.out.println("Categoria: " + custom.getCategory());
      System.out.println("Aiuto: " + custom.getHelp());
      System.out.println("Nome: " + custom.getName());      
		System.out.println(o);
	}
	*/
   

	/** 
	Restituisce la categoria LibreOffice;
	Vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
	ad esempio, si puo' restituire "Data&Orario" oppure "Foglio elettronico"
	*/
	@Override
	public String getCategory()
	{
		return "SOCIAL";
	}

	/** Informazioni di aiuto */
	@Override
	public String getHelp()
	{
		return "La funzione permette di seguire un determinato utente passato come parametro su Twitter";
	} 

	/** 
	Nome della funzione.
	vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
	ad es. "VAL.DISPARI" 
	*/
	@Override         
	public String getName()
	{
		return "createFriendship";
	}   
}
