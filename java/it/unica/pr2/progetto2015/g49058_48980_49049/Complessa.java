/*
 *		FUNZIONE COMPLESSA - CALCOLO DELL'AMMORTAMENTO
 */

package it.unica.pr2.progetto2015.g49058_48980_49049;
import it.unica.pr2.progetto2015.interfacce.SheetFunction;


public class Complessa implements SheetFunction{
    
	//costruttore vuoto
	public Complessa(){}

	@Override
	public Object execute(Object... args) 
	{
		/*** DICHIARAZIONE E INIZIALIZZAZIONE VARIABILI ***/

		/* 
		 * 	Dichiariamo le variabili a cui assegnamo il valore dei parametri della funzione ammortamento:
		 * 	in ordine il costo, il valore residuo, la vita utile, il periodo e il fattore. 
		 */   
		Double costo = (Double)args[0]; 	// rappresenta il costo iniziale del bene 

		Double valRes = (Double)args[1]; 	// rappresente il valore ottenuto alla fine dell'ammortamento

		Integer vitaUtile = (Integer)args[2];   // rappresente il numero di periodi(in anni) in cui il bene viene ammortizzato

		Double periodo = (Double)args[3]; 	/* periodo per il quale si calcola l'ammortamento 
														 * (es. se periodo=2 l'ammortamento sarà relativo al secondo anno) */
		Double fattore = 2.00; // rappresenta il tasso di deprezzamento del valore residuo che vale 2 di default

		Double valAmmort = 0.00;	 // rappresenta l'ammortamento annuo; conterrà il risultato della nostra funzione

		Double totAmmort = 0.00; 	 // rappresenta il fondo ammortamento, cioè il totale degli ammortamenti fino al periodo specificato

		String valoreAmmort;
		String fattoreApp = (String)args[4];

		/*	Il campo fattore è facoltativo quindi può anche non essere inserito e in questo caso assumerà automaticamente il valore di default 
		 * cioè 2; il metodo con fattore uguale a 2 corrisponde al metodo di ammortamento a doppie quote proporzionali ai valori residui. 
		 */	
		
		if(fattoreApp.equals("") || fattoreApp.equals(" ") || fattoreApp.equals("no"))  
		// controllo sel'utente ha inserito il fattore oppure ha lasciato il campo vuoto ("")													     
		{
			fattore = 2.00; // se il campo è vuoto allora assegnamo a fattore il valore 2; 
		}
		else
		{
			fattore = Double.parseDouble(fattoreApp); // se il campo non è vuoto assegnamo il valore inserito dall'utente a fattore
			if(fattore <= 0)
			{
				return "Errore, il valore del fattore e' uguale o inferiore a zero";
			}
		}
		
		
		/*** CONTROLLI PER IL VALORE DELLE VARIABILI ***/
	
		/* 
		 *	Controlliamo che tutti i valori inseriti dall'utente siano superiori a 0 (ad eccezione del valore residuo che può valere anche 0)
		 *	altrimenti segnaliamo l'errore 
		 */	
		if(costo <= 0 || vitaUtile <= 0 || periodo <= 0 || valRes < 0)
		{
			return (Object) "Errore, uno o più dei valori passati sono uguali o inferiori a zero";
		}
			
		/*
		 *	Controlliamo che il periodo inserito dall'utete sia minore o uguale alla vita utile del bene altrimenti segnaliamo l'errore. 
		 *	Se venisse inserito un periodo superiore alla vita utile sarebbe impossibile poter calcolare l'ammortamento poichè il valore
		 *	del bene si è esaurito con la vita utile.	
		 */				
		if( vitaUtile < periodo)
		{
			return (Object) "Errore, la vita utile è minore del periodo";
		}

		/*	Controlliamo che il valore residuo inserito dall'utente sia inferiore o al massimo uguale al costo iniziale del bene
		 *	altrimenti segnaliamo l'errore 
		 */
		if( costo < valRes)
		{
			return (Object) "Errore, il costo è minore del valore residuo";
		}	


		/*** CALCOLO AMMORTAMENTO ***/

		/* 	
		 *	Per calcolare il valore dell'ammortamento corrispondente al periodo inserito usiamo:
		 *
		 *	- un ciclo for che si ferma una volta che i raggiunge il valore del periodo 
		 *
		 *	- ad ogni ciclo viene calcolato l'ammortamento annuo sottraendo il fondo degli ammortamenti (il quale viene incrementato ogni giro 
		 *	  aggiungendo l'ammortamento del periodo) al costo iniziale del bene e moltiplicando poi questa differenza per il rapporto tra il
		 *	  fattore e la vita utile del bene
		 * 
		 *	- oppure sottraendo al costo inizale sia il fondo ammoramento che il valore residuo a seconda del valore ottenuto 
		 *	  da ciascuna formula: viene scelto il valore minimo mediante la funzione min della libreria java.lang.Math. 
		 *
		 *	Si usa questo procedimento poichè quando il valore ottenuto dalla seconda formula sarà inferiore a quello ottenuto dalla prima
		 *	vorrà dire che si è arrvati all'ultimo ciclo in cui l'ammortamento deve essere calcolato tenendo conto del valore residuo
		 *	inserito dall'utente. 
		 *
		 */
		
				
		for(int i = 0; i < periodo; i++)
		{			
			valAmmort = Math.min((costo - totAmmort) * (fattore/vitaUtile), (costo - valRes - totAmmort)); 
			totAmmort = totAmmort + valAmmort;
		}
		
      		valoreAmmort = "" + valAmmort;
		/*
		 *	Restituiamo al programma chiamante il valore dell'ammortamento relativo al periodo desiderato castato ad Object poichè il tipo 
		 *	di valore di ritorno del metodo execute è Object mentre valAmmort è Double 
		 */
		
		return (Object) valoreAmmort;
	
	}
	
	/*** MAIN - per testare la funzione fuori dal foglio LibreOffice ***/
  
	/*
	public static void main(String[] args) {

        Double costo = 25000.00;
        Double valRes = 23.00;
        Integer vitaUtile = 5;
        Double periodo = 3.00;
        String fattore = " ";
        String valAmmort; 
        Complessa complessa = new Complessa();
        
        valAmmort = (String) complessa.execute(costo, valRes, vitaUtile, periodo, fattore);
        
        System.out.println("Categoria: " + complessa.getCategory());
        System.out.println("Aiuto: " + complessa.getHelp());
        System.out.println("Nome: " + complessa.getName());        
        System.out.println(valAmmort + " : Ammortamento per il " + periodo.intValue() + "° periodo");
	}
    */
	
	
	/** 
	Restituisce la categoria LibreOffice;
	Vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
	ad esempio, si puo' restituire "Data&Orario" oppure "Foglio elettronico"
	*/
	@Override
	public String getCategory()
	{
		return "Finanza";
	}

	/** Informazioni di aiuto */
	@Override
	public String getHelp()
	{
		return "Restituisce l'ammortamento degressivo aritmetico per un periodo specificato. Il valore di ammortamento parte da un valore più alto e poi si riduce da un periodo di ammortamento all'altro";
	}

	/** 
	Nome della funzione.
	vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
	ad es. "VAL.DISPARI" 
	*/
	@Override         
	public String getName()
	{
		return "AMMORT";
	}   
    
}
